from django.contrib import admin
from django.urls import include, path


urlpatterns = [

    path('tareas/', include('tareas.urls')),
    path('admin/', admin.site.urls),

]