from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from .models import Tarea


def index(request):
    lista_ultimas_tareas = list(Tarea.objects.all())
    return render(request, 'tareas/index.html', {'lista_ultimas_tareas': lista_ultimas_tareas})
